<?php
require_once 'config/db.php';
require_once 'libs/database.php';
require 'vendor/autoload.php';

function processInput($uri) {
    $uri = implode(
            '/',
            array_slice(
                explode('/', $uri), 1));  
    return $uri;
}

function processOutput($response) {
    echo json_encode($response);    
}

function getPDOInstance() {
    global $host, $username,$password, $dbname;
    return Databse::getInstance($host, $username,$password, $dbname);
}

require_once 'routerApi.php';
//processOutput($response);