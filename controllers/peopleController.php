<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PeopleController {
    private $pdo;
    
    public function __construct($pdo) {
        $this->pdo = $pdo;
    }
    
    function readAll() {
        $sql = "SELECT *
                FROM people
                LIMIT 10";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    function readOne($id) {
        $sql = "SELECT *
                FROM people
                WHERE id = ?";
        $params = [$id];
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll();
    }
    
    function insert(array $insertData) {
        $peaces = [];
        $params = [];
        
        if ($insertData) {
            foreach ($insertData as $key => $value) {
                array_push($peaces, $key .  ' = ?');
                array_push($params, $value);
            }
            $insertStr = implode(',', $peaces);

            $sql = "INSERT INTO people 
                    SET $insertStr";
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($params);
                $res = [
                    'status' => 'ok',
                    'message' => 'Запись добавлена'
                ];
            } catch (PDOException $e) {
                $res = [
                    'status' => 'error',
                    'message' => 'Ошибка при добавлении записи'
                ];
            }
        } else {
            $res = [
                'status' => 'error',
                'message' => 'Данные не были переданы'
            ];
        }
        return $res;
    }
    
    function update($id, array $insertData) {
        $peaces = [];
        $params = [];

        if ($insertData) {
            foreach ($insertData as $key => $value) {
                array_push($peaces, $key .  ' = ?');
                array_push($params, $value);
            }
            $insertStr = implode(',', $peaces);
            array_push($params, $id);
            $sql = "UPDATE people 
                    SET $insertStr
                    WHERE id = ?";
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($params);
                $res = [
                    'status' => 'ok',
                    'message' => 'Запись обновлена'
                ];
            } catch (PDOException $e) {
                $res = [
                    'status' => 'error',
                    'message' => 'Ошибка при обновлении записи'
                ];
            }
        } else {
            $res = [
                'status' => 'error',
                'message' => 'Данные не были переданы'
            ];
        }
        return $res;
    }
    
    function delete($id) {
        if ($id) {
            $sql = "DELETE FROM people
                    WHERE id = ?";
            $params = [$id];
            try {
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($params);
                $res = [
                    'status' => 'ok',
                    'message' => 'Запись удалена'
                ];
            } catch (PDOException $e) {
                $res = [
                    'status' => 'error',
                    'message' => 'Ошибка при удалении записи'
                ];
            }
        } else {
            $res = [
                'status' => 'error',
                'message' => 'Не выбран элемент для удаления'
            ];
        }
        return $res;
    }

}