<?php
class Databse {
    public static $instance;
    private static $errorMessage;

    protected function __construct(){}

    public static function getInstance($ghost, $guser, $gpwd, $gdbname)
    {
        if (empty(self::$instance)) {
            $options = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => true,
                PDO::MYSQL_ATTR_FOUND_ROWS   => true
            );
            try {
                $dsn = 'mysql:host=' . $ghost . ';dbname=' . $gdbname . ';charset=utf8';
                self::$instance = new PDO($dsn, $guser, $gpwd, $options);
            } catch(PDOException $e) {
                self::setErrorMessage('Couldn\'t connect to DB: ' . $e->getMessage());
                return false;
            }
        }

        return self::$instance;

    }

    private static function setErrorMessage($message)
    {
        self::$errorMessage = $message;
    }

    public static function getErrorMessage()
    {
        return self::$errorMessage;
    }
}


