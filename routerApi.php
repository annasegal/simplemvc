<?php
require_once 'controllers/peopleController.php';
$router = new Phroute\RouteCollector(new Phroute\RouteParser);
$router->get('/', function() { 
    return 'root';
});

$router->get('people', function() {
    $object = new PeopleController(getPDOInstance());
    $res = $object->readAll(); 
    require_once 'views/getPeople.php';
    
});

$router->get('people/{id}', function($id) {
    $object = new PeopleController(getPDOInstance());
    $res = $object->readOne($id);
    require_once 'views/getPeople.php';
});

$router->post('people', function() {
    $object = new PeopleController(getPDOInstance());
    return $object->insert($_REQUEST);
});

$router->put('people/{id}', function($id) {
    $object = new PeopleController(getPDOInstance());
    $post_vars = [];
    parse_str(file_get_contents("php://input"),$post_vars);
    return $object->update($id, $post_vars);
});

$router->delete('people/{id}', function ($id) {
        $object = new PeopleController(getPDOInstance());
        return $object->delete($id);
});

$router->get('login', function() {
    global $validUsers;
    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
        $user = $_SERVER['PHP_AUTH_USER'];
        $pass = $_SERVER['PHP_AUTH_PW'];
        if (key_exists($user, $validUsers) && $pass == $validUsers[$user]) {
            //some view
        }
    }
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
    return ['status' => 'error','message' => 'Not authorized'];
});



$dispatcher = new Phroute\Dispatcher($router);
try {
    $response = $dispatcher->dispatch(
        $_SERVER['REQUEST_METHOD'], 
        processInput($_SERVER['REQUEST_URI'])
    );
} catch (Phroute\Exception\HttpRouteNotFoundException $e) {
    var_dump($e);      
    die();

} catch (Phroute\Exception\HttpMethodNotAllowedException $e) {
    var_dump($e);       
    die();

}
